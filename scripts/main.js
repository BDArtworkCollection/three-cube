
// Setup global vars
var fieldOfView = 60;
var aspect = window.innerWidth / window.innerHeight;
var nearClippingPlane = 1;
var farClippingPlane = 1000;
var cube;

// Create scene obj
var scene = new THREE.Scene();

// Create camera
var camera = new THREE.PerspectiveCamera(fieldOfView, aspect, nearClippingPlane, farClippingPlane);

// Create render
var renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);

// Create domElement obj
document.body.appendChild(renderer.domElement);

// Create 3D obj
var geometry = new THREE.BoxGeometry( 1, 1, 1);

// Add material Wireframe
var material = new THREE.MeshNormalMaterial({ wireframe: true });

// Create Mesh
var cube = new THREE.Mesh(geometry, material);

// Add scene
scene.add( cube );
    // Move camera
    camera.position.z = 5;

// Render scene (function)
var render = function() {
    requestAnimationFrame(render);
    cube.rotation.x += 0.01;
    cube.rotation.z -= 0.01;
    renderer.render(scene, camera);
};
    render();

// Window resize
window.addEventListener('resize', function(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
},
    false
);

// Rotation
cube.rotation.set(0, Math.PI/180 * 45, Math.PI/180 * -25);
cube.position.set(1, .1, -.1);

// Position
cube.position.x = 2 * Math.sin(cube.rotation.x);
cube.position.z = 2 * Math.sin(cube.rotation.z);